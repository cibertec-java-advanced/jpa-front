package pe.edu.cibertec.view.managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import pe.edu.cibertec.dominio.Cliente;
import pe.edu.cibertec.dominio.Usuario;
import pe.edu.cibertec.repositorio.ClienteRepositorio;
import pe.edu.cibertec.repositorio.UsuarioRepositorio;
import pe.edu.cibertec.repositorio.impl.ClienteJpaRepositorioImpl;
import pe.edu.cibertec.repositorio.impl.UsuarioJpaRepositorioImpl;

@ManagedBean
@SessionScoped
public class LoginBean {

    private String usuario;
    private String clave;
    private Cliente cliente;

    public String login() {
        EntityManagerFactory emf = (EntityManagerFactory)FacesContext
            .getCurrentInstance()
            .getExternalContext()
            .getApplicationMap().get("emf");
        
        EntityManager em = emf.createEntityManager();
        UsuarioRepositorio usuarioRepositorio = new UsuarioJpaRepositorioImpl().setEm(em);
        Usuario us = usuarioRepositorio.iniciarSesion(this.usuario, this.clave);
        em.close();
        
        if(us != null){
            EntityManager em2 = emf.createEntityManager();
            ClienteRepositorio clienteRepositorio = new ClienteJpaRepositorioImpl().setEm(em2);
            cliente = clienteRepositorio.buscarPorUsuario(us.getId());
            em2.close();
            
            clave = null;
        }
        return us == null ? "" : "productos.xhtml?faces-redirect=true";
    }

    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }   
    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
