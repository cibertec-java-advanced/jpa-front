package pe.edu.cibertec.view.managedbeans;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import pe.edu.cibertec.dominio.Producto;
import pe.edu.cibertec.repositorio.ProductoRepositorio;
import pe.edu.cibertec.repositorio.impl.ProductoJpaRepositorioImpl;

@ManagedBean
@ViewScoped
public class ProductosBean {
    private List<Producto> lstProducto;

    public ProductosBean(){
    }
    
    @PostConstruct //Tiene que ser void, public, no excepciones, no argumentos
    public void init(){
        EntityManagerFactory emf = (EntityManagerFactory)FacesContext
            .getCurrentInstance()
            .getExternalContext()
            .getApplicationMap().get("emf");
        EntityManager em = emf.createEntityManager();
        
        ProductoRepositorio productoRepositorio = new ProductoJpaRepositorioImpl().setEntityManager(em);
        lstProducto = productoRepositorio.obtenerTodos();
        em.close();
    }

    public List<Producto> getLstProducto() {
        return lstProducto;
    }
    public void setLstProducto(List<Producto> lstProducto) {
        this.lstProducto = lstProducto;
    }
}
